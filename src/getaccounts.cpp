/*************************************************************************************
 *  Copyright (C) 2019 by Rituka Patwal <ritukapatwal21@gmail.com>                   * 
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/
 
#include "getaccounts.h"  

#include "synccontacts.h"

#include "getcredentialsjob.h"
#include "core.h"

#include <KJob>
#include <KConfigCore/KSharedConfig>
#include <KConfigCore/KConfigGroup>
#include <Accounts/Service>
#include <Accounts/Manager>
#include <Accounts/Account>
#include <Accounts/AccountService>     
#include <QDebug>
#include <QUrl>

GetKAccounts::GetKAccounts()
{
}

GetKAccounts::~GetKAccounts()
{
}

void GetKAccounts::getAccountIdList()
{
    Accounts::AccountIdList accountList = KAccounts::accountsManager()->accountListEnabled(QStringLiteral("dav-contacts"));
    // quint32 accountId = accountList.first();
    qDebug()<<"Account List: "<<accountList;
    // getCredentials(accountId);
    Q_FOREACH (const quint32 accountId, accountList) {
        qDebug() << "Account IDs : "<<accountId;
        getCredentials(accountId);
    }
}
        

     
void GetKAccounts::getCredentials(const Accounts::AccountId accountId)
{
    GetCredentialsJob *credentialsJob = new GetCredentialsJob(accountId, this);
    connect(credentialsJob, &GetCredentialsJob::finished, this, &GetKAccounts::getAccountDetails);
    credentialsJob->start();
}

void GetKAccounts::getAccountDetails(KJob *job)
{
    GetCredentialsJob *credentialsJob = qobject_cast<GetCredentialsJob*>(job);
    job->deleteLater();

    const QVariantMap &data = credentialsJob->credentialsData();
    m_account = KAccounts::accountsManager()->account(credentialsJob->accountId());

    QUrl carddavUrl = m_account->value("carddavUrl").toUrl();

    qDebug() <<"Using: host:"<<carddavUrl.host()<< " " <<m_account->value("carddavUrl");

    const QString &userName = data.value("AccountUsername").toString();
    const QString &password = data.value("Secret").toString();
    qDebug()<<"username : "<<userName<<"\npassword : ";

    m_account->setValue("resourceid","ID123");

    qDebug()<<" \n\nRESOURCE ID " <<m_account->value("resourceid")<<"\n\n\n\n";

    SyncContacts *synccontact = new SyncContacts(credentialsJob->accountId(), carddavUrl.host(), userName, password);
    synccontact->createResource();
}
