/*************************************************************************************
 *  Copyright (C) 2019 by Rituka Patwal <ritukapatwal21@gmail.com>                   * 
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#include <sink/store.h>
#include <sink/secretstore.h>
#include <sink/resourcecontrol.h>
#include "synccontacts.h"
#include <QDebug>
#include <QUrl>
#include <QUuid>

#include "getcredentialsjob.h"
#include "core.h"

SyncContacts::SyncContacts()
{
}

SyncContacts::SyncContacts(quint32 accountId, QUrl serverUrl, QString userName, QString password)
    :   m_accountId(accountId), 
        m_serverUrl(serverUrl),
        m_userName(userName),
        m_password(password)
{
}

SyncContacts::~SyncContacts()
{
}

void SyncContacts::createResource(){ 
    QByteArray b;
    b.setNum(m_accountId);
    auto resource = Sink::ApplicationDomain::CardDavResource::create(b);

    resource.setProperty("server","https://" + m_serverUrl.toString());
    resource.setProperty("username", m_userName);
    Sink::SecretStore::instance().insert(resource.identifier(), m_password);
    Store::create(resource).exec().waitForFinished();
    qDebug()<<"***CONTACT CREATED***";
    
    mResourceInstanceIdentifier = resource.identifier();
    synchContact();
    return;
}

void SyncContacts::synchContact()
{
    qDebug()<<"\n\n\n 11 ";
    //start resourcecontrol
    Sink::ResourceControl::start(mResourceInstanceIdentifier);

    //Sync Addressbooks
    Sink::SyncScope scope1;
    scope1.setType<Addressbook>();
    scope1.resourceFilter(mResourceInstanceIdentifier);
    Store::synchronize(scope1).exec().waitForFinished();
    qDebug()<<"\n\n\n 11 ";

    //flush
    Sink::ResourceControl::flushMessageQueue(mResourceInstanceIdentifier).exec().waitForFinished();
    qDebug()<<"***FLUSHED***";

    //Sync Contacts
    Sink::SyncScope scope2;
    scope2.setType<Sink::ApplicationDomain::Contact>();
    scope2.resourceFilter(mResourceInstanceIdentifier);
    Store::synchronize(scope2).exec().waitForFinished();
    qDebug()<<"***CONTACT SYNCED***";

    //flush
    Sink::ResourceControl::flushMessageQueue(mResourceInstanceIdentifier).exec().waitForFinished();
    qDebug()<<"***FLUSHED***";
    countContacts();
    return;
}

void SyncContacts::countContacts()
{
    const auto contacts = Sink::Store::read<Sink::ApplicationDomain::Addressbook>(Sink::Query().resourceFilter(mResourceInstanceIdentifier));
    // QCOMPARE(contacts.size(), 2);
    qDebug()<<"Contacts count  :  "<<contacts.size() << contacts.first().resourceInstanceIdentifier();
    return;
}

// void createContact(const QString &firstname, const QString &lastname, const QString &collectionName)
// {
//     QUrl mainUrl(QStringLiteral("http://localhost/dav/addressbooks/user/doe"));
//     mainUrl.setUserName(QStringLiteral("doe"));
//     mainUrl.setPassword(QStringLiteral("doe"));

//     KDAV2::DavUrl davUrl(mainUrl, KDAV2::CardDav);

//     auto *job = new KDAV2::DavCollectionsFetchJob(davUrl);
//     job->exec();

//     const auto collectionUrl = [&] {
//         for (const auto &col : job->collections()) {
//             if (col.displayName() == collectionName) {
//                 return col.url().url();
//             }
//         }
//         return QUrl{};
//     }();


//     QUrl url{collectionUrl.toString() + firstname + lastname + ".vcf"};
//     url.setUserInfo(mainUrl.userInfo());
//     KDAV2::DavUrl testItemUrl(url, KDAV2::CardDav);
//     QByteArray data = QString("BEGIN:VCARD\r\nVERSION:3.0\r\nPRODID:-//Kolab//iRony DAV Server 0.3.1//Sabre//Sabre VObject 2.1.7//EN\r\nUID:12345678-1234-1234-%1-%2\r\nFN:%1 %2\r\nN:%2;%1;;;\r\nEMAIL;TYPE=INTERNET;TYPE=HOME:%1.%2@example.com\r\nREV;VALUE=DATE-TIME:20161221T145611Z\r\nEND:VCARD\r\n").arg(firstname).arg(lastname).toUtf8();
//     KDAV2::DavItem item(testItemUrl, QStringLiteral("text/vcard"), data, QString());
//     auto createJob = new KDAV2::DavItemCreateJob(item);
//     createJob->exec();
//     if (createJob->error()) {
//         qWarning() << createJob->errorString();
//     }
// }