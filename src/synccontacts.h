/*************************************************************************************
 *  Copyright (C) 2019 by Rituka Patwal <ritukapatwal21@gmail.com>                   * 
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/
 
#include <sink/store.h>
#include <QObject>
#include <QUrl>

using namespace Sink;
using namespace Sink::ApplicationDomain;
using Sink::ApplicationDomain::SinkResource;

class SyncContacts : public QObject
{
    Q_OBJECT
    QByteArray mResourceInstanceIdentifier;
    quint32 m_accountId; 
    QUrl m_serverUrl; 
    QString m_userName; 
    QString m_password;


public:
    SyncContacts();
    SyncContacts(quint32 accountId, QUrl serverUrl, QString userName, QString password);
    ~SyncContacts();

public Q_SLOTS:
    void createResource();
    void synchContact();
    void countContacts();
};