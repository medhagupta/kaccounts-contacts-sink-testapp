/*************************************************************************************
 *  Copyright (C) 2019 by Rituka Patwal <ritukapatwal21@gmail.com>                   * 
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#ifndef GETKACCOUNTS_H
#define GETKACCOUNTS_H

#include <KConfigCore/KSharedConfig>
#include <KConfigCore/KConfigGroup>
#include <KJob>
#include <Accounts/Service>
#include <Accounts/Manager>
#include <Accounts/Account>
#include <Accounts/AccountService>   

class GetKAccounts : public QObject
{
    Q_OBJECT
    // KSharedConfig::Ptr config;

    //FOR SINK
        QByteArray mResourceInstanceIdentifier;
    Accounts::Account *m_account;

public:
    GetKAccounts();
    ~GetKAccounts();
    void getCredentials(const Accounts::AccountId accountId);

    void createResource(quint32 accountId, QUrl serverUrl, QString userName, QString password);
    void synchContact();
    void countContacts();

public Q_SLOTS:
    void getAccountIdList();
    void getAccountDetails(KJob *job);
};

#endif